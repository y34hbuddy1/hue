package hue

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
)

type HueClient struct {
	BridgeHost string
	BridgeUser string
}

type HueLights map[string]HueLight

type HueLight struct {
	State            HueLightState        `json:"state"`
	SwUpdate         HueLightSwUpdate     `json:"swupdate"`
	Type             string               `json:"type"`
	Name             string               `json:"name"`
	ModelId          string               `json:"modelid"`
	ManufacturerName string               `json:"manufacturername"`
	ProductName      string               `json:"productname"`
	Capabilities     HueLightCapabilities `json:"capabilities"`
	Config           HueLightsConfig      `json:"config"`
	UniqueId         string               `json:"uniqueid"`
	SwVersion        string               `json:"swversion"`
	SwConfigId       string               `json:"swconfigid"`
	ProductId        string               `json:"productid"`
}

type HueLightState struct {
	On        bool   `json:"on"`
	Bri       int    `json:"bri"`
	Alert     string `json:"alert"`
	Mode      string `json:"mode"`
	Reachable bool   `json:"reachable"`
}

type HueLightSwUpdate struct {
	State       string `json:"state"`
	LastInstall string `json:"lastinstall"`
}

type HueLightCapabilities struct {
	Certified bool                          `json:"certified"`
	Control   HueLightCapabilitiesControl   `json:"control"`
	Streaming HueLightCapabilitiesStreaming `json:"streaming"`
}

type HueLightCapabilitiesControl struct {
	MinDimLevel int `json:"mindimlevel"`
	MaxLumen    int `json:"maxlumen"`
}

type HueLightCapabilitiesStreaming struct {
	Renderer bool `json:"renderer"`
	Proxy    bool `json:"proxy"`
}

type HueLightsConfig struct {
	Archetype string `json:"archetype"`
	Function  string `json:"function"`
	Direction string `json:"direction"`
}

type HueGroups map[string]HueGroup

type HueGroup struct {
	Name    string         `json:"name"`
	Lights  []string       `json:"lights"`
	Type    string         `json:"type"`
	State   HueGroupState  `json:"state"`
	Recycle bool           `json:"recycle"`
	Class   string         `json:"class"`
	Action  HueGroupAction `json:"action"`
}

type HueGroupState struct {
	AllOn bool `json:"all_on"`
	AnyOn bool `json:"any_on"`
}

type HueGroupAction struct {
	On        bool   `json:"on"`
	Bri       int    `json:"bri"`
	Ct        int    `json:"ct"`
	Alert     string `json:"alert"`
	ColorMode string `json:"colormode"`
}

func buildAPIBaseUrl(host string, user string) string {
	return "http://" + host + "/api/" + user
}

func (c HueClient) hueAPIRequest(uri string, method string, body string) (responseBody string, err error) {
	bodyReader := strings.NewReader(body)

	url := buildAPIBaseUrl(c.BridgeHost, c.BridgeUser) + uri

	req, _ := http.NewRequest(method, url, bodyReader)

	customClient := http.Client{
		CheckRedirect: func(redirRequest *http.Request, via []*http.Request) error {
			// Go's http.DefaultClient does not forward headers when a redirect 3xx
			// response is received. Thus, the header (which in this case contains the
			// Authorization token) needs to be passed forward to the redirect
			// destinations.
			redirRequest.Header = req.Header

			// Go's http.DefaultClient allows 10 redirects before returning an
			// an error. We have mimicked this default behavior.
			if len(via) >= 10 {
				return errors.New("stopped after 10 redirects")
			}
			return nil
		},
	}

	response, _ := customClient.Do(req)
	respBody, _ := ioutil.ReadAll(response.Body)

	if response.StatusCode != 200 {
		return "", fmt.Errorf(
			"expected a 200 status code; got a %d; error: %s",
			response.StatusCode,
			string(respBody))
	}

	defer response.Body.Close()

	return string(respBody), nil
}

func (c HueClient) GetAllLights() (lights HueLights, err error) {
	respBody, err := c.hueAPIRequest("/lights", http.MethodGet, "")

	if err != nil {
		return lights, err
	}

	err = json.Unmarshal([]byte(respBody), &lights)

	if err != nil {
		return lights, err
	}

	return lights, nil
}

func (c HueClient) GetAllGroups() (groups HueGroups, err error) {
	respBody, err := c.hueAPIRequest("/groups", http.MethodGet, "")

	if err != nil {
		return groups, err
	}

	err = json.Unmarshal([]byte(respBody), &groups)

	if err != nil {
		return groups, err
	}

	return groups, nil
}

func (c HueClient) SetGroupOnOff(groupId int, on bool) error {
	boolStr := "true"
	if on == false {
		boolStr = "false"
	}

	respBody, err := c.hueAPIRequest("/groups/"+strconv.Itoa(groupId)+"/action", http.MethodPut, "{\"on\": "+boolStr+"}")

	if err != nil {
		return errors.New("Failed to set group state: " + err.Error() + "\nServer response: " + respBody)
	}

	return nil
}

func (c HueClient) TurnGroupOff(groupId int) error {
	return c.SetGroupOnOff(groupId, false)
}

func (c HueClient) TurnGroupOn(groupId int) error {
	return c.SetGroupOnOff(groupId, true)
}
