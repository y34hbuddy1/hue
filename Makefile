CC=go build
SOURCES=hue.go
LIBRARY=hue.a

all: $(SOURCES) $(LIBRARY)

$(LIBRARY): $(SOURCES)
	go test -cover
	GOOS=linux $(CC) $(SOURCES)

install:
	GOOS=linux go install

clean:
	@rm -fv $(LIBRARY)
